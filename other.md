# Other Sardana related projects

Below you will find a list of Sardana related projects e.g. GUIs, widgets, tools, utilities, etc.

| Name | Description | Link(s) to project |
| ---- | ----------- | ------------ |
| LaVue | An online live viewer to show 2D live images from xray-detectors  | [**LaVue**](https://github.com/lavue-org/lavue) |
| sardana_change_tangodb | Script for changing Tango database in Sardana persistent information | [**sardana_change_tangodb**](https://gist.github.com/reszelaz/c20ff2cf8c505cf7622c6e6537219da9) |
| sardana_plugin_inventory | Script to interrogate sardana servers to extract info about 3rd party plugins (macros or controllers) | [**sardana_plugin_inventory**](https://gist.github.com/1dfc10ca1caa5b0123483d1dfa57d18c.git) |
| Sherlock | Checks Pool's health and structure | [**Sherlock**](https://gitlab.com/MaxIV/app-maxiv-sherlock) |

